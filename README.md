# Estruturas de Dados e seus Algoritmos

Curso de verão ministrado em Janeiro de 2024 no Instituto de Computação da Universidade Federal Fluminense.

## Sobre o curso

### Quando

Segundas a Quintas de 08/Jan/2024 a 08/Fev/2024, 09h às 13h.

Laboratório nos dias **11/Jan**, **17/Jan**, **25/Jan** e **31/Jan**

### Onde

Instituto de Computação, Niterói-RJ.

Sala 217, Laboratório 302.

### Quem

Professor: Pedro Cortez F. Lopes

email: [pedro@ic.uff.br](pedro@ic.uff.br)

sala: Por enquanto, Laboratório 507.

### Ementa

+ Árvores
    + Binárias
    + Binárias de Busca
    + AVL
    + B e B+
+ Grafos
+ Arquivos
    + Acesso de dados em disco
    + Ordenação externa de dados em memória secundária
+ Tabelas hash
+ Heaps

### Avaliação

Duas provas: P1 e P2. Agendadas para **18/Jan** e **01/Fev**, respecitvamente.

$N = \frac{P1 + P2}{2}$

**Aprovação**: $N\geqslant 6$

**VS**: $4\leqslant N\lt 6$

**Reprovação**: $N\lt 4$

**Obs.:** Presença mínima de 75\% será exigida. Se faltar mais de 25\% das aulas sem justificativa cabível, o aluno será reprovado.
[Ver regulamento](https://www.uff.br/sites/default/files/001-2015_regulamento_do_curso_de_graduacao_0.pdf).

## Links úteis

+ [Documentação de C](https://devdocs.io/c/)
+ [Vídeo-aulas da professora V. Braganholo](https://www.youtube.com/playlist?list=PLYqZErLVzMfGDF0M2p-ZKKq0xDdGWuvqI)
+ [Valgrind](https://valgrind.org/) -- ferramenta para a detecção de problemas no gerenciamento da memória em C. Específico para Linux.
+ [MinGW](https://sourceforge.net/projects/mingw/) -- compilador de C para Windows. Para Linux, usem diretamente o GCC (padrão).
+ [Árvore AVL interativa](https://www.cs.usfca.edu/~galles/visualization/AVLtree.html)
+ [Árvore B interativa](https://www.cs.usfca.edu/~galles/visualization/BTree.html)

## Agradecimentos

Agradeço às professoras [V. Braganholo](https://www.ic.uff.br/blog/pessoas/vanessa-braganholo-murta/) e [I. Rosseti](https://www.ic.uff.br/blog/pessoas/isabel-cristina-mello-rosseti/) pelo ótimo material cedido que muito me auxiliou na preparação deste curso.
