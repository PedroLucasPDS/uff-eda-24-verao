#ifndef MY_LSE_H_INCLUDE
#define MY_LSE_H_INCLUDE

#include <stdio.h>
#include <stdlib.h>

typedef struct _lse{
    int dado;
    struct _lse *prox;
} lse_t;

lse_t *lse_inicia(void);
void lse_libera(lse_t *l);
void lse_imprime(lse_t *l);

lse_t *lse_insere(lse_t *l, int d);
lse_t *lse_insere_no_inicio(lse_t *l, int d);
lse_t *lse_insere_no_fim(lse_t *l, int d);
lse_t *lse_insere_ordenado(lse_t *l, int d);

lse_t *lse_remove(lse_t *l, lse_t **anteprox);
lse_t *lse_remove_no_inicio(lse_t *l);
lse_t *lse_remove_no_fim(lse_t *l);

lse_t *lse_busca(lse_t *l, int d);
lse_t *lse_busca_min(lse_t *l);
lse_t *lse_busca_max(lse_t *l);

lse_t *lse_ordena(lse_t *l);
lse_t *lse_merge_ordenado(lse_t *l1, lse_t *l2);
lse_t *lse_ordena_mergesort(lse_t *l);

#endif // MY_LSE_H_INCLUDE
