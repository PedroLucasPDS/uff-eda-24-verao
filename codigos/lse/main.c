#include <stdio.h>
#include <stdlib.h>
#include "lse.h"

int main(void){

	unsigned int n;
	printf("Tamanho do vetor:\n");
	scanf("%u",&n);

	int *v = (int *)malloc(sizeof(int)*n);
	if (!v) { printf("Não consegui alocar.\n"); return 1; }

	printf("Dados:\n");
	for (int i=0; i<n; i++) scanf("%d",&v[i]);

	lse_t *lista = lse_inicia();
	for (int i=0; i<n; i++) lista = lse_insere_no_fim(lista, v[i]);
	free(v);
	
	lse_imprime(lista);

	lista = lse_insere_no_inicio(lista,2);
	//lista = lse_insere_no_fim(lista,1);

	lse_imprime(lista);
	
	lse_t *no = lse_busca(lista,10);
	if (no){
	  printf("Achou 10 na lista");
	  no->prox ? printf(", antes de %d\n",no->prox->dado) : printf(", no final.\n");
	} else printf("Não achou 10 na lista.\n");
	
	no = lse_busca_min(lista);
	printf("Min: %d\n",no->dado);
	no = lse_busca_max(lista);
	printf("Max: %d\n",no->dado);

	lista = lse_ordena(lista);
	//lista = lse_ordena_mergesort(lista);
	
	lse_imprime(lista);

	lse_libera(lista);

	return 0;
}
