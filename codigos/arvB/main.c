#include <stdio.h>
#include <stdlib.h>

#include "arvB.h"

int main(void){

  // Exercicio 1 dos slides de ArvB
  const int ordem = 2;

  arvb_t *raiz = arvb_inicia();
  
  int arr[] = {1,2,15,20,9,3,4,15,20,30,40,46,50,51,52,60,65,70,56,58,80,85,90};
  const int arr_dim = sizeof(arr)/sizeof(int);
  for (int i=0; i<arr_dim; i++) raiz = arvb_insere(raiz,arr[i],ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  
  raiz = arvb_insere(raiz,57,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_insere(raiz,71,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_insere(raiz,72,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_insere(raiz,73,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_remove(raiz,15,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_remove(raiz,20,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_insere(raiz,10,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_remove(raiz,1,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_remove(raiz,50,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_remove(raiz,70,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_remove(raiz,73,ordem);
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  raiz = arvb_remove(raiz,65,ordem);
  raiz = arvb_remove(raiz,72,ordem);
  raiz = arvb_remove(raiz,90,ordem);
  
  arvb_imprime(raiz,0);
  
  arvb_libera(raiz);

  // Exercicios 2 e 3 dos slides de ArvB
  /*
  const int ordem = 3;

  arvb_t *raiz = arvb_inicia();
  
  raiz = arvb_insere(raiz,8,ordem);
  raiz = arvb_insere(raiz,1,ordem);
  raiz = arvb_insere(raiz,6,ordem);
  raiz = arvb_insere(raiz,3,ordem);
  raiz = arvb_insere(raiz,14,ordem);
  raiz = arvb_insere(raiz,36,ordem);
  raiz = arvb_insere(raiz,32,ordem);
  raiz = arvb_insere(raiz,43,ordem);
  raiz = arvb_insere(raiz,39,ordem);
  raiz = arvb_insere(raiz,41,ordem);
  raiz = arvb_insere(raiz,38,ordem);
  
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  
  raiz = arvb_insere(raiz,4,ordem);
  raiz = arvb_insere(raiz,5,ordem);
  raiz = arvb_insere(raiz,42,ordem);
  raiz = arvb_insere(raiz,2,ordem);
  raiz = arvb_insere(raiz,7,ordem);
  
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  
  raiz = arvb_remove(raiz,14,ordem);
  
  arvb_imprime(raiz,0);
  printf("\n##########################\n\n");
  
  raiz = arvb_remove(raiz,32,ordem);
  
  arvb_imprime(raiz,0);
  
  arvb_libera(raiz);
  */
  return 0;
}
