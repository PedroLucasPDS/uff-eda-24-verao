#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _aluno{
  int mat;
  char nome[128];
  unsigned int nota;
} aluno_t;


void le_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fscanf(f,"%d %s %d",&a->mat,a->nome,&a->nota);
}

void salva_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fprintf(f,"%d %s %d\n",a->mat,a->nome,a->nota);
}

void imprime_aluno(aluno_t *a){
  if (!a) return;
  printf("[%d] %s: %d\n",a->mat,a->nome,a->nota);
}

void imprime_conteudo_arquivo(FILE *f, size_t n){
  if (!f || !n) return;
  aluno_t *dados = (aluno_t *)malloc(sizeof(aluno_t)*n);
  for (int i=0; i<n; i++) le_aluno(f,&dados[i]);
  for (int i=0; i<n; i++) imprime_aluno(&dados[i]);
  free(dados);
}

size_t numero_de_linhas(FILE *f){
  if (!f) return 0;
  rewind(f);
  size_t n=0;
  int ch, prev_ch=-1;
  while(1){
    ch = fgetc(f);
    if (ch==EOF){
      if (prev_ch != '\n') n++;
      break;
    }
    if (ch=='\n') n++;
    prev_ch=ch;
  }
  rewind(f);
  return n;
}

typedef struct _pilha_aluno{
  aluno_t *a;
  FILE *f;
  struct _pilha_aluno *prox;
} pilha_t;

pilha_t *push_ordenado(pilha_t *p, aluno_t *a, FILE *f){
  if (!a || !f) return p;
  pilha_t *novo = (pilha_t *)malloc(sizeof(pilha_t));
  novo->a=a;
  novo->f=f;
  novo->prox=NULL;
  if (!p) return novo;
  if (p->a->nota <= a->nota) { novo->prox=p; return novo; }
  pilha_t *aux = p;
  while (aux->prox){
    if (aux->prox->a->nota <= a->nota){
      novo->prox = aux->prox;
      break;
    }
    aux = aux->prox;
  }
  aux->prox = novo;
  return p;
}

pilha_t *pop(pilha_t *p){
  if (!p) return NULL;
  pilha_t *aux=p;
  p=p->prox;
  if (aux->a) free(aux->a); aux->a=NULL;
  free(aux);
  return p;
}

aluno_t *copia_aluno(aluno_t *a){
  aluno_t *pa = (aluno_t *)malloc(sizeof(aluno_t));
  pa->mat = a->mat;
  pa->nota = a->nota;
  strcpy(pa->nome,a->nome);
  return pa;
}

void intercala_particoes_pilha(FILE *f, FILE **particoes, size_t np){
  if (!f || !particoes || !np) return;
  pilha_t *candidatos=NULL;
  aluno_t a;
  FILE *pf;
  for (int i=0; i<np; i++){
    le_aluno(particoes[i],&a);
    candidatos = push_ordenado(candidatos,copia_aluno(&a),particoes[i]);
  }
  while(candidatos){
    pf = candidatos->f;
    salva_aluno(f,candidatos->a);
    candidatos = pop(candidatos);
    le_aluno(pf,&a);
    if (!feof(pf)){
      candidatos = push_ordenado(candidatos,copia_aluno(&a),pf);
    }
  }
}

int main(int argc, char *argv[]){
  
  //////////////////////////////////////////////////////
//  FILE *parts[] = {
//    fopen("particao_0.txt","r"),
//    fopen("particao_1.txt","r")
//  };
//  const int np = sizeof(parts) / sizeof(FILE *);
//  for (int i=0; i<np; i++){
//    if (!parts[i]){
//      for (int j=0; j<np; j++) if (parts[j]) fclose(parts[j]);
//      return 1;
//    }
//  }
  if (argc<2) return 1;
  const int np = argc-1;
  FILE **parts = (FILE **)malloc(sizeof(FILE *)*np);
  for (int i=0; i<np; i++){
    printf("%s\n",argv[i+1]);
    parts[i] = fopen(argv[i+1],"r");
    if (!parts[i]){
      for (int j=(i-1); j>=0; j--) fclose(parts[j]);
      free(parts);
      return 1;
    }
  }
  
  FILE *f = fopen("resultado_intercalacao.txt","w");
  if (!f) {free(parts); return 1;}
  intercala_particoes_pilha(f,parts,np);
  fclose(f);
  
  for (int i=0; i<np; i++) fclose(parts[i]);
  free(parts);
  //////////////////////////////////////////////////////
  
  return 0;
}
