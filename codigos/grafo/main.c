#include <stdio.h>
#include "grafo.h"

#define FALSE 0
#define TRUE 1

int main(void){

  grf_vertice_t *g = grf_inicia();
  
  g = grf_insere_vertice(g,5);
  g = grf_insere_vertice(g,4);
  g = grf_insere_vertice(g,3);
  g = grf_insere_vertice(g,2);
  g = grf_insere_vertice(g,1);
  
  grf_insere_aresta(g,1,2,TRUE);
  grf_insere_aresta(g,1,3,TRUE);
  grf_insere_aresta(g,2,3,FALSE);
  grf_insere_aresta(g,2,4,FALSE);
  grf_insere_aresta(g,3,4,TRUE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,1,5) ? "" : "nao",1,5);
  grf_imprime(g); printf("\n");
  
  grf_insere_aresta(g,5,4,TRUE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,1,5) ? "" : "nao",1,5);
  grf_imprime(g); printf("\n");
  
  grf_remove_aresta(g,1,2,FALSE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,1,5) ? "" : "nao",1,5);
  grf_imprime(g); printf("\n");
  
  g = grf_remove_vertice(g,1);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,2,5) ? "" : "nao",2,5);
  grf_imprime(g); printf("\n");
  
  grf_remove_aresta(g,4,3,TRUE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,2,5) ? "" : "nao",2,5);
  grf_imprime(g); printf("\n");
  
  g = grf_remove_vertice(g,4);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,2,5) ? "" : "nao",2,5);
  grf_imprime(g);
  
  grf_libera(g);

  return 0;
}
