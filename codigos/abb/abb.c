#include "abb.h"

abb_t *abb_busca(abb_t *a, int d){
  if(!a) return NULL;
  if (d == a->dado) return a;
  if(d < a->dado)
    return abb_busca(a->esq,d);
  return abb_busca(a->dir,d);
}

abb_t *abb_insere(abb_t *a, int d){
  if(!a){
    abb_t *no=(abb_t *)malloc(sizeof(abb_t));
    no->dado=d;
    no->esq=NULL; no->dir=NULL;
    return no;
  }
  if(d < a->dado)
    a->esq=abb_insere(a->esq,d);
  else if(d > a->dado)
    a->dir=abb_insere(a->dir,d);
  return a;
}

abb_t *abb_remove(abb_t *a, int d){
  if(!a)return NULL;
  if(a->dado!=d){
    if(d < a->dado) a->esq=abb_remove(a->esq,d);
    else a->dir=abb_remove(a->dir,d);
    return a;
  }
  if(!(a->esq) && !(a->dir)){ free(a); return NULL;}
  if(!(a->esq) || !(a->dir)){
    abb_t *p=a;
    a = a->esq ? a->esq : a->dir;
    free(p);
  }else{
    abb_t *p=a->esq;
    while(p->dir) p=p->dir;
    a->dado=p->dado;
    p->dado=d;
    a->esq=abb_remove(a->esq,d);
  }
  return a;
}
