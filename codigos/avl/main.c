#include <stdio.h>
#include "avl.h"

int main(void){
  avl_t *raiz = avl_insere(NULL,42);
  raiz = avl_insere(raiz,15);
  raiz = avl_insere(raiz,88);
  raiz = avl_insere(raiz,6);
  raiz = avl_insere(raiz,27);
  
  avl_imprime_tab(raiz,0);
  
  printf("\n");
  
  raiz = avl_insere(raiz,34);
  
  avl_imprime_tab(raiz,0);
  
  printf("\n");
  
  raiz = avl_insere(raiz,5);
  
  avl_imprime_tab(raiz,0);
  
  printf("\n");
  
  raiz = avl_insere(raiz,22);
  raiz = avl_insere(raiz,25);
  
  avl_imprime_tab(raiz,0);
  
  printf("\n");
  
  raiz = avl_insere(raiz,16);
  
  avl_imprime_tab(raiz,0);
  avl_imprime_altura_tab(raiz,0);
  avl_imprime_fb_tab(raiz,0);
  
  printf("\n");
  
  raiz = avl_remove(raiz,15);
  raiz = avl_remove(raiz,5);
  
  avl_imprime_tab(raiz,0);
  avl_imprime_altura_tab(raiz,0);
  avl_imprime_fb_tab(raiz,0);
  
  printf("\n");
  
  raiz = avl_remove(raiz,42);
  raiz = avl_remove(raiz,34);
  
  avl_imprime_tab(raiz,0);
  avl_imprime_altura_tab(raiz,0);
  avl_imprime_fb_tab(raiz,0);
  
  avl_libera(raiz);
  return 0;
}
