#include <stdio.h>
#include <stdlib.h>

#include "utils/lse.h"
#include "utils/ab.h"
#include "utils/abb.h"
#include "utils/avl.h"
#include "utils/grafo.h"

#define FALSE 0
#define TRUE 1

int grf_tem_caminho_bfs(grf_vertice_t *g, unsigned int id1, unsigned int id2){
  grf_vertice_t *v1 = grf_busca_vertice(g,id1);
  grf_vertice_t *v2 = grf_busca_vertice(g,id2);
  if (!v1 || !v2) return 0;
  
  unsigned int max_id=g->id;
  grf_vertice_t *pg=g->prox;
  while(pg) {
    max_id = pg->id > max_id ? pg->id : max_id;
    pg=pg->prox;
  }
  
  int *visitados = (int *)malloc(sizeof(int)*max_id);
  for (int i=0;i<max_id;i++) visitados[i]=0;
  visitados[v1->id-1]=1;
  
  int achou_caminho=0;
  lse_vertice_t *fila, *fim_da_fila=NULL, *pv;
  fila = (lse_vertice_t *)malloc(sizeof(lse_vertice_t));
  fila->vertice = v1;
  fila->prox = NULL;
  fim_da_fila=fila;
  while(fila){
    pg = fila->vertice;
    if (pg == v2){
      achou_caminho=1;
      break;
    }
    visitados[pg->id-1]=1;
    pv = pg->vizinho;
    while(pv){
      pg = pv->vertice;
      if ( !visitados[pg->id-1] ){
        fim_da_fila->prox = (lse_vertice_t *)malloc(sizeof(lse_vertice_t));
        fim_da_fila = fim_da_fila->prox;
        fim_da_fila->vertice = pg;
        fim_da_fila->prox = NULL;
      }
      pv=pv->prox;
    }
    pv = fila;
    fila=fila->prox;
    free(pv);
  }
  
  
  free(visitados);
  while(fila){
    pv=fila;
    fila=fila->prox;
    free(pv);
  }
  
  return achou_caminho;
}

int main(void){
  
  grf_vertice_t *g = grf_inicia();
  
  g = grf_insere_vertice(g,5);
  g = grf_insere_vertice(g,4);
  g = grf_insere_vertice(g,3);
  g = grf_insere_vertice(g,2);
  g = grf_insere_vertice(g,1);
  
  grf_insere_aresta(g,1,2,TRUE);
  grf_insere_aresta(g,1,3,TRUE);
  grf_insere_aresta(g,2,3,FALSE);
  grf_insere_aresta(g,2,4,FALSE);
  grf_insere_aresta(g,3,4,TRUE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,1,5) ? "" : "nao",1,5);
  grf_imprime(g); printf("\n");
  
  printf("Insere {5,4}\n");
  grf_insere_aresta(g,5,4,TRUE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,1,5) ? "" : "nao",1,5);
  grf_imprime(g); printf("\n");
  
  printf("Remove (1,2)\n");
  grf_remove_aresta(g,1,2,FALSE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,1,5) ? "" : "nao",1,5);
  grf_imprime(g); printf("\n");
  
  printf("Remove 1\n");
  g = grf_remove_vertice(g,1);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,2,5) ? "" : "nao",2,5);
  grf_imprime(g); printf("\n");
  
  printf("Remove {4,3}\n");
  grf_remove_aresta(g,4,3,TRUE);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,2,5) ? "" : "nao",2,5);
  grf_imprime(g); printf("\n");
  
  printf("Remove 4\n");
  g = grf_remove_vertice(g,4);
  
  printf("%s tem caminho entre %d e %d\n",grf_tem_caminho_bfs(g,2,5) ? "" : "nao",2,5);
  grf_imprime(g);
  
  grf_libera(g);

  return 0;
}
