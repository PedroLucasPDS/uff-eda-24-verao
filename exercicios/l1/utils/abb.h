#ifndef MY_ABB_H_INCLUDE
#define MY_ABB_H_INCLUDE

#include <stdio.h>
#include <stdlib.h>
#include "ab.h"

typedef ab_t abb_t;

abb_t *abb_busca(abb_t *a, int d);
abb_t *abb_insere(abb_t *a, int d);
abb_t *abb_remove(abb_t *a, int d);

#endif // MY_ABB_H_INCLUDE
