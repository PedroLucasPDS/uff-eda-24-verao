#ifndef MY_AVL_H_INCLUDE
#define MY_AVL_H_INCLUDE

#include <stdio.h>
#include <stdlib.h>

typedef struct _avl{
    int dado;
    struct _avl *esq;
    struct _avl *dir;
    int fb, alt;
} avl_t;

avl_t *avl_insere(avl_t *a, int d);
void avl_libera(avl_t *a);

avl_t *avl_busca(avl_t *a, int d);
avl_t *avl_remove(avl_t *a, int d);

unsigned int avl_calcula_altura(avl_t *a);
unsigned int avl_calcula_fb(avl_t *a);

void avl_imprime_tab(avl_t *a, const unsigned int tab);
void avl_imprime_preordem(avl_t *a);

void avl_imprime_altura_tab(avl_t *a, const unsigned int tab);
void avl_imprime_altura_preordem(avl_t *a);

void avl_imprime_fb_tab(avl_t *a, const unsigned int tab);
void avl_imprime_fb_preordem(avl_t *a);

avl_t *avl_RD(avl_t *a);
avl_t *avl_RE(avl_t *a);
avl_t *avl_RDE(avl_t *a);
avl_t *avl_RED(avl_t *a);

#endif // MY_AVL_H_INCLUDE
