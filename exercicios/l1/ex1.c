#include <stdio.h>
#include <stdlib.h>

#include "utils/lse.h"
#include "utils/ab.h"
#include "utils/abb.h"
#include "utils/avl.h"
#include "utils/grafo.h"

abb_t *abb_de_vetor_ordenado_recursivo(abb_t *a, int *v, unsigned int n){
  if (!v || !n) return a;
  unsigned int meio=n/2;
  a = abb_insere(a,v[meio]);
  a = abb_de_vetor_ordenado_recursivo(a,v,meio);
  if ((meio+1)<n) a = abb_de_vetor_ordenado_recursivo(a,&v[meio+1],(n-1)/2);
  return a;
}

abb_t *abb_de_vetor_ordenado(int *v, unsigned int n){
  return abb_de_vetor_ordenado_recursivo(NULL,v,n);
}

avl_t *avl_de_vetor_ordenado(int *v, unsigned int n){
  if (!v || !n) return NULL;
  
  typedef struct _fila {
    int *v;
    unsigned int n;
    struct _fila *prox;
  } fila_t;
  
  fila_t *push(fila_t *f, int *v, unsigned int n){
    fila_t *fim = (fila_t *)malloc(sizeof(fila_t));
    fim->v = v;
    fim->n = n;
    fim->prox = NULL;
    if (f) f->prox = fim;
    return fim;
  }
  
  fila_t *pop(fila_t *f){
    if (!f) return NULL;
    fila_t *p = f;
    f=f->prox;
    free(p);
    return f;
  }
  
  avl_t *a=NULL;
  fila_t *fila = push(NULL,v,n);
  fila_t *fim = fila;
  unsigned int meio;
  while(fila){
    meio = fila->n / 2;
    a = avl_insere(a,fila->v[meio]);
    if (meio > 0) fim = push(fim,fila->v,meio);
    if ((meio+1) < fila->n) fim = push(fim,&(fila->v[meio+1]),(fila->n-1)/2);
    fila = pop(fila);
  }
  return a;
}

int main(void){

  //int arr[] = {1,2,3,4,5,6,7};
  
  int arr[] = {15, 56, 66, 133, 165, 182, 240, 273, 315, 328, 345, 358, 369, 374, 387, 408, 431, 470, 520, 538, 637, 652, 729, 730, 860, 905, 912, 928, 934, 955, 962, 977, 979, 991, 992};
  
  abb_t *raiz = abb_de_vetor_ordenado(arr,sizeof(arr)/sizeof(int));
  printf("abb: ");ab_imprime_preordem(raiz);printf("\n");
  ab_libera(raiz);
  
  avl_t *a = avl_de_vetor_ordenado(arr,sizeof(arr)/sizeof(int));
  printf("avl: ");avl_imprime_preordem(a);printf("\n");
  avl_libera(a);
  
  return 0;
}
