#include <stdio.h>
#include <stdlib.h>

#include "utils/lse.h"
#include "utils/ab.h"
#include "utils/abb.h"
#include "utils/avl.h"
#include "utils/grafo.h"

#define FALSE 0
#define TRUE 1

int **grf_matriz_adjacencia(grf_vertice_t *g){
  if (!g) return NULL;
  unsigned int n=0;
  grf_vertice_t *pg, *pg_aux;
  lse_vertice_t *pv;
  for (pg=g; pg; pg=pg->prox) n++;
  int **m = (int **)malloc(sizeof(int *)*n);
  m[0] = (int *)malloc(sizeof(int)*n*n);
  for (unsigned int i=1; i<n; i++) m[i]=&(m[0][i*n]);
  for (unsigned int i=0; i<(n*n); i++) m[0][i]=0;
  pg=g;
  unsigned int j;
  for (unsigned int i=0; i<n; i++){
    for (pv=pg->vizinho; pv; pv=pv->prox){
      j=n;
      pg_aux=g;
      for(unsigned int k=0; k<n && pg_aux; k++){
        if (pg_aux == pv->vertice){
          j=k;
          break;
        }
        pg_aux=pg_aux->prox;
      }
      if (j<n) m[i][j]=1;
    }
    pg=pg->prox;
  } 
  return m;
}

grf_vertice_t *grf_de_matriz_adjacencia(int **m, unsigned int n){
  if (!m) return NULL;
  grf_vertice_t *g = grf_inicia(), *pg;
  for (unsigned int i=0; i<n; i++) g = grf_insere_vertice(g,i+1);
  for (unsigned int i=0; i<n; i++){
    for (unsigned int j=0; j<n; j++){
      if(m[i][j]) grf_insere_aresta(g,i+1,j+1,FALSE);
    }
  }
  return g;
}

void grf_libera_matriz_adjacencia(int **m){
  if (!m) return;
  free(m[0]);
  free(m);
}

int main(void){
  
  grf_vertice_t *g = grf_inicia();
  
  g = grf_insere_vertice(g,5);
  g = grf_insere_vertice(g,4);
  g = grf_insere_vertice(g,3);
  g = grf_insere_vertice(g,2);
  g = grf_insere_vertice(g,1);
  
  grf_insere_aresta(g,1,2,TRUE);
  grf_insere_aresta(g,1,3,TRUE);
  grf_insere_aresta(g,2,3,FALSE);
  grf_insere_aresta(g,2,4,FALSE);
  grf_insere_aresta(g,3,4,TRUE);
  
  grf_imprime(g); printf("\n");
  
  printf("Insere {5,4}\n");
  grf_insere_aresta(g,5,4,TRUE);

  grf_imprime(g); printf("\n");
  
  printf("Remove (1,2)\n");
  grf_remove_aresta(g,1,2,FALSE);

  grf_imprime(g); printf("\n");
  
  int **matriz = grf_matriz_adjacencia(g);
  
  unsigned int i=0, j=0;
  for (grf_vertice_t *pi=g; pi; pi=pi->prox){
    for (grf_vertice_t *pj=g; pj; pj=pj->prox){
      printf(" %d",matriz[i][j++]);
    }
    i++;
    j=0;
    printf("\n");
  }
  
  grf_vertice_t *novo_g = grf_de_matriz_adjacencia(matriz,i);
  printf("\n"); grf_imprime(novo_g); printf("\n");
  
  grf_libera_matriz_adjacencia(matriz);
  grf_libera(g);
  grf_libera(novo_g);

  return 0;
}
