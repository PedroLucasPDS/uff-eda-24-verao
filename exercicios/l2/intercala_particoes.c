#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _aluno{
  int mat;
  char nome[128];
  unsigned int nota;
} aluno_t;


void le_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fscanf(f,"%d %s %d",&a->mat,a->nome,&a->nota);
}

void salva_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fprintf(f,"%d %s %d\n",a->mat,a->nome,a->nota);
}

void imprime_aluno(aluno_t *a){
  if (!a) return;
  printf("[%d] %s: %d\n",a->mat,a->nome,a->nota);
}

void imprime_conteudo_arquivo(FILE *f, size_t n){
  if (!f || !n) return;
  aluno_t *dados = (aluno_t *)malloc(sizeof(aluno_t)*n);
  for (int i=0; i<n; i++) le_aluno(f,&dados[i]);
  for (int i=0; i<n; i++) imprime_aluno(&dados[i]);
  free(dados);
}

size_t numero_de_linhas(FILE *f){
  if (!f) return 0;
  rewind(f);
  size_t n=0;
  int ch, prev_ch=-1;
  while(1){
    ch = fgetc(f);
    if (ch==EOF){
      if (prev_ch != '\n') n++;
      break;
    }
    if (ch=='\n') n++;
    prev_ch=ch;
  }
  rewind(f);
  return n;
}

//////////////////////////////////////////////////////////////////////////
// Tipo no arv vencedores
typedef struct _no_abv{
  aluno_t *vencedor;
  struct _no_abv *p_vencedor;
  FILE *f;
  struct _no_abv *esq, *dir, *pai;
} abv_t;

// Tipo fila de nos de arv vencedores
typedef struct _fila_abv{
  abv_t *a;
  struct _fila_abv *prox;
} fila_abv_t;

fila_abv_t *push_fila(fila_abv_t *fila, abv_t *a){
  if (!a) return fila;
  fila_abv_t *novo = (fila_abv_t *)malloc(sizeof(fila_abv_t));
  if (fila){
    fila->prox=novo;
    fila=fila->prox;
  } else fila=novo;
  fila->a=a;
  fila->prox=NULL;
  return fila;
}

fila_abv_t *pop_fila(fila_abv_t *fila){
  if (!fila) return NULL;
  fila_abv_t *aux = fila;
  fila=fila->prox;
  free(aux);
  return fila;
}

// Funcao que cria pai:
//    [novo] --> determina vencedor entre esq e dir
//     /  \
//   esq  dir
abv_t *novo_no_arvbin_vencedores(abv_t *esq, abv_t *dir){
  if (!esq || !dir) return NULL;
  abv_t *novo = (abv_t *)malloc(sizeof(abv_t));
  if (esq->vencedor->nota < 11 && dir->vencedor->nota < 11){ // high value
    if (esq->vencedor->nota > dir->vencedor->nota){
      novo->vencedor = esq->vencedor;
      novo->p_vencedor = esq->p_vencedor;
      novo->f = esq->f;
    } else {
      novo->vencedor = dir->vencedor;
      novo->p_vencedor = dir->p_vencedor;
      novo->f = dir->f;
    }
  } else if (dir->vencedor->nota >= 11){
    novo->vencedor = esq->vencedor;
    novo->p_vencedor = esq->p_vencedor;
    novo->f = esq->f;
  } else {
    novo->vencedor = dir->vencedor;
    novo->p_vencedor = dir->p_vencedor;
    novo->f = dir->f;
  }
  novo->esq=esq;
  novo->dir=dir;
  novo->pai=NULL;
  esq->pai=novo;
  dir->pai=novo;
  return novo;
}

abv_t *cria_arvbin_vencedores(FILE **particoes, size_t np){
  if (!particoes || !np) return NULL;
  fila_abv_t *inicio=NULL, *fim=NULL;
  abv_t **folhas = (abv_t **)malloc(sizeof(abv_t *)*np);
  for (int i=0;i<np;i++){
    folhas[i] = (abv_t *)malloc(sizeof(abv_t));
    folhas[i]->vencedor = (aluno_t *)malloc(sizeof(aluno_t));
    le_aluno(particoes[i],folhas[i]->vencedor);
    if (feof(particoes[i])) folhas[i]->vencedor->nota=11; // high value
    folhas[i]->p_vencedor = folhas[i];
    folhas[i]->f = particoes[i];
    folhas[i]->pai=NULL;
    folhas[i]->esq=NULL;
    folhas[i]->dir=NULL;
  }
  inicio = push_fila(NULL,folhas[0]); fim=inicio;
  for (int i=1;i<np;i++) fim = push_fila(fim,folhas[i]);
  while (fim != inicio){
    fim = push_fila(fim,novo_no_arvbin_vencedores(inicio->a,inicio->prox->a));
    inicio=pop_fila(inicio);
    inicio=pop_fila(inicio);
  }
  free(folhas);
  abv_t *raiz = inicio->a;
  while(inicio) inicio=pop_fila(inicio);
  return raiz;
}

void atualiza_arvbin_vencedores(abv_t *a){
  if (!a) return;
  abv_t *no = a->p_vencedor;
  le_aluno(no->f,no->vencedor);
  if (feof(no->f)) no->vencedor->nota=11; // high value
  while(no->pai){
    no=no->pai;
    if (no->esq->vencedor->nota < 11 && no->dir->vencedor->nota < 11){
      if (no->esq->vencedor->nota > no->dir->vencedor->nota){
        no->vencedor = no->esq->vencedor;
        no->p_vencedor = no->esq->p_vencedor;
        no->f = no->esq->f;
      } else {
        no->vencedor = no->dir->vencedor;
        no->p_vencedor = no->dir->p_vencedor;
        no->f = no->dir->f;
      }
    } else if(no->dir->vencedor->nota >= 11){
      no->vencedor = no->esq->vencedor;
      no->p_vencedor = no->esq->p_vencedor;
      no->f = no->esq->f;
    } else {
      no->vencedor = no->dir->vencedor;
      no->p_vencedor = no->dir->p_vencedor;
      no->f = no->dir->f;
    }
  }
}

void libera_arvbin_vencedores(abv_t *a){
  if (!a) return;
  if (!(a->esq) && !(a->dir)) free(a->vencedor);
  else{
    libera_arvbin_vencedores(a->esq);
    libera_arvbin_vencedores(a->dir);
  }
  free(a);
}

void intercala_particoes_arvbin_vencedores(FILE *f, FILE **particoes, size_t np){
  if (!f || !particoes || !np) return;
  abv_t *raiz = cria_arvbin_vencedores(particoes,np);
  while(raiz->vencedor->nota < 11){ // 11 sendo usado como "high value"
    salva_aluno(f,raiz->vencedor);
    atualiza_arvbin_vencedores(raiz);
  }
  libera_arvbin_vencedores(raiz);
}
//////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]){
  
  //////////////////////////////////////////////////////
  if (argc<2) return 1;
  const int np = argc-1;
  FILE **parts = (FILE **)malloc(sizeof(FILE *)*np);
  for (int i=0; i<np; i++){
    printf("%s\n",argv[i+1]);
    parts[i] = fopen(argv[i+1],"r");
    if (!parts[i]){
      for (int j=(i-1); j>=0; j--) fclose(parts[j]);
      free(parts);
      return 1;
    }
  }
  
  FILE *f = fopen("resultado_intercalacao.txt","w");
  if (!f) {free(parts); return 1;}
  intercala_particoes_arvbin_vencedores(f,parts,np);
  fclose(f);
  
  for (int i=0; i<np; i++) fclose(parts[i]);
  free(parts);
  //////////////////////////////////////////////////////
  
  return 0;
}
