#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/arvB.h"

int arvb_max(arvb_t *a){
  return 0; // placeholder. remover quando fizer o exercicio
  /*
  EXERCICIO: Completar
  */
}

int arvb_min(arvb_t *a){
  return 0; // placeholder. remover quando fizer o exercicio
  /*
  EXERCICIO: Completar
  */
}

int arvb_maiorque(arvb_t *a, int x){
  return 0; // placeholder. remover quando fizer o exercicio
  /*
  EXERCICIO: Completar
  */
}

int main(void){

  const int ordem = 2;

  arvb_t *raiz = arvb_inicia();
  
  int arr[] = {1,2,15,20,9,3,4,15,20,30,40,46,50,51,52,60,65,70,56,58,80,85,90,57,71,72,73};
  const int arr_dim = sizeof(arr)/sizeof(int);
  for (int i=0; i<arr_dim; i++) raiz = arvb_insere(raiz,arr[i],ordem);
  arvb_imprime(raiz,0);
  printf("\n");
  
  // CHAMADA DAS FUNCOES
  printf("chave max: %d\n",arvb_max(raiz));
  printf("chave min: %d\n",arvb_min(raiz));
  int x=46;
  printf("chave imediatamente maior que %d: %d\n",x,arvb_maiorque(raiz,x));
  
  arvb_libera(raiz);

  return 0;
}
