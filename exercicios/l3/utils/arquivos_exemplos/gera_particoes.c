#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _aluno{
  int mat;
  char nome[128];
  unsigned int nota;
} aluno_t;


void le_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fscanf(f,"%d %s %d",&a->mat,a->nome,&a->nota);
}

void salva_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fprintf(f,"%d %s %d\n",a->mat,a->nome,a->nota);
}

void imprime_aluno(aluno_t *a){
  if (!a) return;
  printf("[%d] %s: %d\n",a->mat,a->nome,a->nota);
}

void imprime_conteudo_arquivo(FILE *f, size_t n){
  if (!f || !n) return;
  aluno_t *dados = (aluno_t *)malloc(sizeof(aluno_t)*n);
  for (int i=0; i<n; i++) le_aluno(f,&dados[i]);
  for (int i=0; i<n; i++) imprime_aluno(&dados[i]);
  free(dados);
}

size_t numero_de_linhas(FILE *f){
  if (!f)  return 0;
  rewind(f);
  size_t n=0;
  int ch, prev_ch=-1;
  while(1){
    ch = fgetc(f);
    if (ch==EOF){
      if (prev_ch != '\n') n++;
      break;
    }
    if (ch=='\n') n++;
    prev_ch=ch;
  }
  rewind(f);
  return n;
}

void ordena_por_nota_em_memoria_insertion_sort(FILE *f, size_t n){
  if (!f || !n) return;
  aluno_t *turma = (aluno_t *)malloc(sizeof(aluno_t)*n);
  le_aluno(f,turma);
  int j;
  aluno_t a;
  for (int i=1; i<n; i++){
    le_aluno(f,&turma[i]);
    a = turma[i];
    for (j=i; j && turma[j-1].nota <= a.nota; j--) turma[j] = turma[j-1];
    turma[j]=a;
  }
  rewind(f);
  for (int i=0; i<n; i++) salva_aluno(f,&turma[i]);
  free(turma);
  fflush(f);
}

void gera_particoes_ordenacao_interna(FILE *f, size_t n, size_t m){
  if (!f || !n || !m) return;
  size_t np = (n-1) / m + 1;
  aluno_t *turma = (aluno_t *)malloc(sizeof(aluno_t)*m);
  FILE *fp;
  int j;
  aluno_t a;
  char filename[128];
  for (unsigned int part=0; part<np; part++){
    sprintf(filename,"particao_%d.txt",part);
    fp = fopen(filename,"w");
    if (!fp) break;
    le_aluno(f,turma);
    if ( part >= (np-1) && (n%m) ) m = n%m; 
    for (int i=1; i<m; i++){
      le_aluno(f,&turma[i]);
      a = turma[i];
      for (j=i; j && turma[j-1].nota <= a.nota; j--) turma[j] = turma[j-1];
      turma[j]=a;
    }
    for (int i=0; i<m; i++) salva_aluno(fp,&turma[i]);
    fclose(fp);
  }
  free(turma);
}

typedef struct _pilha_aluno{
  aluno_t *a;
  struct _pilha_aluno *prox;
} pilha_t;

pilha_t *push_ordenado(pilha_t *p, aluno_t *a){
  if (!a) return p;
  pilha_t *novo = (pilha_t *)malloc(sizeof(pilha_t));
  novo->a=a;
  novo->prox=NULL;
  if (!p) return novo;
  if (p->a->nota <= a->nota) { novo->prox=p; return novo; }
  pilha_t *aux = p;
  while (aux->prox){
    if (aux->prox->a->nota <= a->nota){
      novo->prox = aux->prox;
      break;
    }
    aux = aux->prox;
  }
  aux->prox = novo;
  return p;
}

pilha_t *pop(pilha_t *p){
  if (!p) return NULL;
  pilha_t *aux=p;
  p=p->prox;
  if (aux->a) free(aux->a); aux->a=NULL;
  free(aux);
  return p;
}

aluno_t *copia_aluno(aluno_t *a){
  aluno_t *pa = (aluno_t *)malloc(sizeof(aluno_t));
  pa->mat = a->mat;
  pa->nota = a->nota;
  strcpy(pa->nome,a->nome);
  return pa;
}

void gera_particoes_selecao_substituicao(FILE *f, size_t n, size_t m){
  if (!f || !n || !m) return;
  pilha_t *dados=NULL, *congelados=NULL;
  aluno_t a;
  char filename[128];
  unsigned int part=0, ultima_nota=11;
  sprintf(filename,"particao_%d.txt",part);
  FILE *fp = fopen(filename,"w");
  if (!fp) return;
  for (int i=0; i<m; i++){
    le_aluno(f,&a);
    if (feof(f)) break;
    dados = push_ordenado(dados,copia_aluno(&a));
  }
  congelados=NULL;
  while(!feof(f)){
    if (dados && dados->a->nota <= ultima_nota){
      salva_aluno(fp,dados->a);
      ultima_nota = dados->a->nota;
      dados = pop(dados);
      le_aluno(f,&a);
      if (feof(f)) break;
      dados = push_ordenado(dados,copia_aluno(&a));
    } else if (dados){
      congelados = push_ordenado(congelados,dados->a);
      dados->a=NULL;
      dados = pop(dados);
    } else {
      fclose(fp);
      dados=congelados;
      congelados=NULL;
      sprintf(filename,"particao_%d.txt",++part);
      fp = fopen(filename,"w");
      if (!fp) break;
      ultima_nota=11;
    }
  }
  if (fp){
    while(dados){
      salva_aluno(fp,dados->a);
      dados = pop(dados);
    }
    fclose(fp);
  } else while(dados) dados = pop(dados);
  if (congelados){
    sprintf(filename,"particao_%d.txt",++part);
    fp = fopen(filename,"w");
    if (fp){
      while(congelados){
        salva_aluno(fp,congelados->a);
        congelados = pop(congelados);
      }
      fclose(fp);
    } else while(congelados) congelados = pop(congelados);
  }
}

void gera_particoes_selecao_natural(FILE *f, size_t n, size_t m){
  if (!f || !n || !m) return;
  pilha_t *dados=NULL;
  aluno_t a;
  char filename[128];
  unsigned int part=0, res_dim=0, ultima_nota=11;
  sprintf(filename,"particao_%d.txt",part);
  FILE *fp = fopen(filename,"w");
  if (!fp) return;
  FILE *fres = fopen("reservatorio.txt","w+");
  if (!fres) {fclose(fp); return;}
  for (int i=0; i<m; i++){
    le_aluno(f,&a);
    if (feof(f)) break;
    dados = push_ordenado(dados,copia_aluno(&a));
  }
  while(!feof(f)){
    if (res_dim >= m){
      while(dados){
        salva_aluno(fp,dados->a);
        dados = pop(dados);
      }
      fclose(fp);
      rewind(fres);
      for (int i=0; i<res_dim; i++){
        le_aluno(fres,&a);
        if (feof(fres)) break;
        dados = push_ordenado(dados,copia_aluno(&a));
      }
      res_dim=0;
      rewind(fres);
      sprintf(filename,"particao_%d.txt",++part);
      fp = fopen(filename,"w");
      if (!fp) break;
      ultima_nota=11;
    } else {
      if (dados->a->nota <= ultima_nota){
        salva_aluno(fp,dados->a);
        ultima_nota = dados->a->nota;
      } else {
        salva_aluno(fres,dados->a);
        fflush(fres);
        res_dim++;
      }
      dados = pop(dados);
      if (res_dim < m){
        le_aluno(f,&a);
        if (feof(f)) break;
        dados = push_ordenado(dados,copia_aluno(&a));
      }
    }
  }
  if (fp){
    while(dados){
      salva_aluno(fp,dados->a);
      dados = pop(dados);
    }
    fclose(fp);
  } else while(dados) dados = pop(dados);
  if (res_dim){
    rewind(fres);
    for (int i=0; i<res_dim; i++){
      le_aluno(fres,&a);
      if (feof(fres)) break;
      dados = push_ordenado(dados,copia_aluno(&a));
    }
    sprintf(filename,"particao_%d.txt",++part);
    fp = fopen(filename,"w");
    if (fp){
      while(dados){
        salva_aluno(fp,dados->a);
        dados = pop(dados);
      }
      fclose(fp);
    } else while(dados) dados = pop(dados);
  }
  fclose(fres);
}

int main(void){

  aluno_t turma[]={
    {0,"Jonas",8},
    {1,"Francisco",3},
    {2,"Marcia",4},
    {3,"Marta",5},
    {4,"Ana",10},
    {5,"Renato",9},
    {6,"Luan",6},
    {7,"Maria",9}
  };
  const int dim = sizeof(turma) / sizeof(aluno_t);
  
  //////////////////////////////////////////////////////
  FILE *f = fopen("turma.txt","w");
  if (!f) return 1;
  for (int i=0; i<dim; i++) salva_aluno(f,&turma[i]);
  fclose(f);
  
  f = fopen("turma.txt","r");
  if (!f) return 1;
  imprime_conteudo_arquivo(f,dim);
  fclose(f);
  //////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////
  f = fopen("turma.txt","r");
  if (!f) return 1;
  size_t n_linhas = numero_de_linhas(f);
  //gera_particoes_ordenacao_interna(f,n_linhas,3);
  //gera_particoes_selecao_substituicao(f,n_linhas,3);
  gera_particoes_selecao_natural(f,n_linhas,3);
  fclose(f);
  //////////////////////////////////////////////////////
  
  return 0;
}
