#include <stdio.h>
#include <stdlib.h>

typedef struct _aluno{
  int mat;
  char nome[128];
  unsigned int nota;
} aluno_t;


void le_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fread(a,sizeof(aluno_t),1,f);
}

void salva_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fwrite(a,sizeof(aluno_t),1,f);
}

void imprime_aluno(aluno_t *a){
  if (!a) return;
  printf("[%d] %s: %d\n",a->mat,a->nome,a->nota);
}

size_t tamanho_arquivo(FILE *f){
  fseek(f,0,SEEK_END);
  size_t tam = ftell(f) / sizeof(aluno_t);
  rewind(f);
  return tam;
}

void imprime_conteudo_arquivo(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo(f);
  if (!n) return;
  aluno_t *dados = (aluno_t *)malloc(sizeof(aluno_t)*n);
  for (int i=0; i<n; i++) le_aluno(f,&dados[i]);
  for (int i=0; i<n; i++) imprime_aluno(&dados[i]);
  free(dados);
}

void ordena_por_nota_em_memoria_insertion_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo(f); // faz um rewind em f
  if (!n) return;
  aluno_t *turma = (aluno_t *)malloc(sizeof(aluno_t)*n);
  le_aluno(f,turma);
  int j;
  aluno_t a;
  for (int i=1; i<n; i++){
    le_aluno(f,&turma[i]);
    a = turma[i];
    for (j=i; j && turma[j-1].nota <= a.nota; j--) turma[j] = turma[j-1];
    turma[j]=a;
  }
  rewind(f);
  for (int i=0; i<n; i++) salva_aluno(f,&turma[i]);
  free(turma);
  fflush(f);
}

void ordena_por_nota_em_disco_insertion_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo(f); // faz um rewind em f
  if (!n) return;
  aluno_t a, b;
  int j;
  for (int i=1; i<n; i++){
    fseek(f,i*sizeof(aluno_t),SEEK_SET);
    le_aluno(f,&a);
    for (j=i; j; j--){
      fseek(f,(j-1)*sizeof(aluno_t),SEEK_SET);
      le_aluno(f,&b);
      if (b.nota > a.nota) break;
      salva_aluno(f,&b);
    }
    fseek(f,j*sizeof(aluno_t),SEEK_SET);
    salva_aluno(f,&a);
    fflush(f);
  }
}

size_t tamanho_arquivo_int(FILE *f){
  fseek(f,0,SEEK_END);
  size_t tam = ftell(f) / sizeof(int);
  rewind(f);
  return tam;
}
void ordena_em_disco_insertion_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo_int(f); // faz um rewind em f
  if (!n) return;
  int a, b;
  int j;
  for (int i=1; i<n; i++){
    fseek(f,i*sizeof(int),SEEK_SET);
    fread(&a,sizeof(int),1,f);
    for (j=i; j; j--){
      fseek(f,(j-1)*sizeof(int),SEEK_SET);
      fread(&b,sizeof(int),1,f);
      if (b > a) break;
      fwrite(&b,sizeof(int),1,f);
    }
    fseek(f,j*sizeof(int),SEEK_SET);
    fwrite(&a,sizeof(int),1,f);
    fflush(f);
  }
}

int compare( const void *arg1, const void *arg2 ){
  int i1 = *(int *)arg1;
  int i2 = *(int *)arg2;
  return i2-i1;
}

int main(void){

  //int arr[] = {15, 56, 66, 133, 165, 182, 240, 273, 315, 328, 345, 358, 369, 374, 387, 408, 431, 470, 520, 538, 637, 652, 729, 730, 860, 905, 912, 928, 934, 955, 962, 977, 979, 991, 992};
  //int arr[] = {12,10,15,16,20,18};
//  int arr[] = {60, 68, 97, 7, 21, 37, 96, 50, 93, 9, 75, 7, 94, 87, 87, 3, 44, 13, 3, 89, 8, 5, 6, 27, 35, 7, 29, 51, 44, 84};
//  const int dim = sizeof(arr) / sizeof(int);
//  FILE *f = fopen("dados.bin","wb");
//  if (!f) return 1;
//  fwrite(arr,sizeof(int),dim,f);
//  fclose(f);
//  qsort(arr,dim,sizeof(int),compare);
//  printf("qsort:"); for (int i=0; i<dim; i++) printf(" %d",arr[i]);
//  printf("\n");
//  
//  f = fopen("dados.bin","rb+");
//  if (!f) return 1;
//  ordena_em_disco_insertion_sort(f);
//  fclose(f);
//  
//  f = fopen("dados.bin","rb");
//  if (!f) return 1;
//  fread(&arr[0],sizeof(int),dim,f);
//  printf("disco:"); for (int i=0; i<dim; i++) printf(" %d",arr[i]);
//  printf("\n");
//  fclose(f);
//
//  return 0;
  
  
  aluno_t turma[]={
    {0,"Jonas",8},
    {1,"Marta",5},
    {2,"Francisco",3},
    {3,"Renato",9},
    {4,"Luan",6},
    {5,"Marcia",4},
    {6,"Maria",9}
  };
  const int dim = sizeof(turma) / sizeof(aluno_t);
  
  //////////////////////////////////////////////////////
  FILE *f = fopen("turma.bin","wb");
  if (!f) return 1;
  for (int i=0; i<dim; i++) salva_aluno(f,&turma[i]);
  fclose(f);
  
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////
  printf("\nOrdenando por nota:\n");
  
  f = fopen("turma.bin","rb+");
  if (!f) return 1;
  //ordena_por_nota_em_memoria_insertion_sort(f);
  ordena_por_nota_em_disco_insertion_sort(f);
  fclose(f);
  
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  
  return 0;
}
