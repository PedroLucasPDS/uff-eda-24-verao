#include "aluno.h"

////////////////////////////////////////////////////////////
void le_aluno_bin(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fread(a,sizeof(aluno_t),1,f);
}
void salva_aluno_bin(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fwrite(a,sizeof(aluno_t),1,f);
}

void le_aluno_txt(FILE *f, aluno_t *a){
  if (!f || !a) return;
  char bufferstr[128] = {0}; bufferstr[0]='\0';
  fscanf(f,"%d %s %d",&a->mat,bufferstr,&a->nota);
  strncpy(a->nome,bufferstr,128);
}
void salva_aluno_txt(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fprintf(f,"%d %s %d\n",a->mat,a->nome,a->nota);
}

void imprime_aluno(aluno_t *a){
  if (!a) return;
  printf("[%d] %s: %d\n",a->mat,a->nome,a->nota);
}

size_t tamanho_arquivo_bin(FILE *f){
  fseek(f,0,SEEK_END);
  size_t tam = ftell(f) / sizeof(aluno_t);
  rewind(f);
  return tam;
}

size_t linhas_arquivo_txt(FILE *f){
  if (!f)  return 0;
  rewind(f);
  size_t n=0;
  int ch, prev_ch=-1;
  while(1){
    ch = fgetc(f);
    if (ch==EOF){
      if (prev_ch != '\n') n++;
      break;
    }
    if (ch=='\n') n++;
    prev_ch=ch;
  }
  rewind(f);
  return n;
}

void imprime_conteudo_arquivo_bin(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo_bin(f);
  if (!n) return;
  aluno_t a;
  for (int i=0; i<n; i++){
    le_aluno_bin(f,&a);
    imprime_aluno(&a);
  }
}

void imprime_conteudo_arquivo_txt(FILE *f){
  if (!f) return;
  size_t n = linhas_arquivo_txt(f);
  if (!n) return;
  aluno_t a;
  for (int i=0; i<n; i++){
    le_aluno_txt(f,&a);
    imprime_aluno(&a);
  }
}
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
void ordena_em_memoria_insertion_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo_bin(f); // faz um rewind em f
  if (!n) return;
  aluno_t *turma = (aluno_t *)malloc(sizeof(aluno_t)*n);
  le_aluno_bin(f,turma);
  int j;
  aluno_t a;
  for (int i=1; i<n; i++){
    le_aluno_bin(f,&turma[i]);
    a = turma[i];
    for (j=i; j && turma[j-1].nota <= a.nota; j--) turma[j] = turma[j-1];
    turma[j]=a;
  }
  rewind(f);
  for (int i=0; i<n; i++) salva_aluno_bin(f,&turma[i]);
  free(turma);
  fflush(f);
}

void ordena_em_memoria_selection_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo_bin(f); // faz um rewind em f
  if (!n) return;
  aluno_t *turma = (aluno_t *)malloc(sizeof(aluno_t)*n);
  fread(turma,sizeof(aluno_t),n,f);
  aluno_t a;
  int i_max;
  for (int i=0; i<(n-1); i++){
    a = turma[i]; i_max=i;
    for (int j=i+1; j<n; j++){
      if (turma[j].nota > a.nota){
        a = turma[j]; i_max=j;
      }
    }
    turma[i_max]=turma[i];
    turma[i]=a;
  }
  rewind(f);
  fwrite(turma,sizeof(aluno_t),n,f);
  fflush(f);
  free(turma);
}

void ordena_em_disco_insertion_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo_bin(f); // faz um rewind em f
  if (!n) return;
  aluno_t a, b;
  int j;
  for (int i=1; i<n; i++){
    fseek(f,i*sizeof(aluno_t),SEEK_SET);
    le_aluno_bin(f,&a);
    for (j=i; j; j--){
      fseek(f,(j-1)*sizeof(aluno_t),SEEK_SET);
      le_aluno_bin(f,&b);
      if (b.nota > a.nota) break;
      salva_aluno_bin(f,&b);
    }
    fseek(f,j*sizeof(aluno_t),SEEK_SET);
    salva_aluno_bin(f,&a);
    fflush(f);
  }
}

void ordena_em_disco_selection_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo_bin(f); // faz um rewind em f
  if (!n) return;
  aluno_t a, b;
  int i_max;
  for (int i=0; i<n; i++){
    fseek(f,i*sizeof(aluno_t),SEEK_SET);
    le_aluno_bin(f,&a); i_max=i;
    for (int j=i+1; j<n; j++){
      le_aluno_bin(f,&b);
      if (b.nota > a.nota){
        a = b; i_max=j;
      }
    }
    if (i_max != i) {
      fseek(f,i*sizeof(aluno_t),SEEK_SET);
      le_aluno_bin(f,&b);
      fseek(f,(i_max)*sizeof(aluno_t),SEEK_SET); 
      salva_aluno_bin(f,&b);
    }
    fseek(f,i*sizeof(aluno_t),SEEK_SET);
    salva_aluno_bin(f,&a);
    fflush(f);
  }
}
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
typedef struct _pilha_aluno{
  aluno_t *a;
  FILE *f;
  struct _pilha_aluno *prox;
} pilha_t;

pilha_t *push_ordenado(pilha_t *p, aluno_t *a, FILE *f){
  if (!a) return p;
  pilha_t *novo = (pilha_t *)malloc(sizeof(pilha_t));
  novo->a=a;
  novo->f=f;
  novo->prox=NULL;
  if (!p) return novo;
  if (p->a->nota <= a->nota) { novo->prox=p; return novo; }
  pilha_t *aux = p;
  while (aux->prox){
    if (aux->prox->a->nota <= a->nota){
      novo->prox = aux->prox;
      break;
    }
    aux = aux->prox;
  }
  aux->prox = novo;
  return p;
}

pilha_t *pop(pilha_t *p){
  if (!p) return NULL;
  pilha_t *aux=p;
  p=p->prox;
  if (aux->a) free(aux->a); aux->a=NULL;
  free(aux);
  return p;
}

aluno_t *copia_aluno(aluno_t *a){
  aluno_t *pa = (aluno_t *)malloc(sizeof(aluno_t));
  pa->mat = a->mat;
  pa->nota = a->nota;
  strncpy(pa->nome,a->nome,sizeof(a->nome));
  return pa;
}

size_t gera_particoes_ordenacao_interna(FILE *f, size_t n, size_t m){
  if (!f || !n || !m) return 0;
  size_t np = (n-1) / m + 1;
  aluno_t *turma = (aluno_t *)malloc(sizeof(aluno_t)*m);
  FILE *fp;
  int j;
  aluno_t a;
  char filename[128];
  for (unsigned int part=0; part<np; part++){
    sprintf(filename,"particao_%d.txt",part);
    fp = fopen(filename,"w");
    if (!fp) break;
    le_aluno_txt(f,turma);
    if ( part >= (np-1) && (n%m) ) m = n%m; 
    for (int i=1; i<m; i++){
      le_aluno_txt(f,&turma[i]);
      a = turma[i];
      for (j=i; j && turma[j-1].nota <= a.nota; j--) turma[j] = turma[j-1];
      turma[j]=a;
    }
    for (int i=0; i<m; i++) salva_aluno_txt(fp,&turma[i]);
    fclose(fp);
  }
  free(turma);
  return np;
}

size_t gera_particoes_selecao_substituicao(FILE *f, size_t n, size_t m){
  if (!f || !n || !m) return 0;
  size_t part=0;
  pilha_t *dados=NULL, *congelados=NULL;
  aluno_t a;
  char filename[128];
  unsigned int ultima_nota=11;
  sprintf(filename,"particao_%zu.txt",part);
  FILE *fp = fopen(filename,"w");
  if (!fp) return 0;
  for (int i=0; i<m; i++){
    le_aluno_txt(f,&a);
    if (feof(f)) break;
    dados = push_ordenado(dados,copia_aluno(&a),NULL);
  }
  congelados=NULL;
  while(!feof(f)){
    if (dados && dados->a->nota <= ultima_nota){
      salva_aluno_txt(fp,dados->a);
      ultima_nota = dados->a->nota;
      dados = pop(dados);
      le_aluno_txt(f,&a);
      if (feof(f)) break;
      dados = push_ordenado(dados,copia_aluno(&a),NULL);
    } else if (dados){
      congelados = push_ordenado(congelados,dados->a,NULL);
      dados->a=NULL;
      dados = pop(dados);
    } else {
      fclose(fp);
      dados=congelados;
      congelados=NULL;
      sprintf(filename,"particao_%zu.txt",++part);
      fp = fopen(filename,"w");
      if (!fp) break;
      ultima_nota=11;
    }
  }
  if (fp){
    while(dados){
      salva_aluno_txt(fp,dados->a);
      dados = pop(dados);
    }
    fclose(fp);
  } else while(dados) dados = pop(dados);
  if (congelados){
    sprintf(filename,"particao_%zu.txt",++part);
    fp = fopen(filename,"w");
    if (fp){
      while(congelados){
        salva_aluno_txt(fp,congelados->a);
        congelados = pop(congelados);
      }
      fclose(fp);
    } else while(congelados) congelados = pop(congelados);
  }
  return part+1;
}

size_t gera_particoes_selecao_natural(FILE *f, size_t n, size_t m){
  if (!f || !n || !m) return 0;
  size_t part=0;
  pilha_t *dados=NULL;
  aluno_t a;
  char filename[128];
  unsigned int res_dim=0, ultima_nota=11;
  sprintf(filename,"particao_%zu.txt",part);
  FILE *fp = fopen(filename,"w");
  if (!fp) return 0;
  FILE *fres = fopen("reservatorio.txt","w+");
  if (!fres) {fclose(fp); return 0;}
  for (int i=0; i<m; i++){
    le_aluno_txt(f,&a);
    if (feof(f)) break;
    dados = push_ordenado(dados,copia_aluno(&a),NULL);
  }
  while(!feof(f)){
    if (res_dim >= m){
      while(dados){
        salva_aluno_txt(fp,dados->a);
        dados = pop(dados);
      }
      fclose(fp);
      rewind(fres);
      for (int i=0; i<res_dim; i++){
        le_aluno_txt(fres,&a);
        if (feof(fres)) break;
        dados = push_ordenado(dados,copia_aluno(&a),NULL);
      }
      res_dim=0;
      rewind(fres);
      sprintf(filename,"particao_%zu.txt",++part);
      fp = fopen(filename,"w");
      if (!fp) break;
      ultima_nota=11;
    } else {
      if (dados->a->nota <= ultima_nota){
        salva_aluno_txt(fp,dados->a);
        ultima_nota = dados->a->nota;
      } else {
        salva_aluno_txt(fres,dados->a);
        fflush(fres);
        res_dim++;
      }
      dados = pop(dados);
      if (res_dim < m){
        le_aluno_txt(f,&a);
        if (feof(f)) break;
        dados = push_ordenado(dados,copia_aluno(&a),NULL);
      }
    }
  }
  if (fp){
    while(dados){
      salva_aluno_txt(fp,dados->a);
      dados = pop(dados);
    }
    fclose(fp);
  } else while(dados) dados = pop(dados);
  if (res_dim){
    rewind(fres);
    for (int i=0; i<res_dim; i++){
      le_aluno_txt(fres,&a);
      if (feof(fres)) break;
      dados = push_ordenado(dados,copia_aluno(&a),NULL);
    }
    sprintf(filename,"particao_%zu.txt",++part);
    fp = fopen(filename,"w");
    if (fp){
      while(dados){
        salva_aluno_txt(fp,dados->a);
        dados = pop(dados);
      }
      fclose(fp);
    } else while(dados) dados = pop(dados);
  }
  fclose(fres);
  return part+1;
}
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
void intercala_particoes_pilha(FILE *f, FILE **particoes, size_t np){
  if (!f || !particoes || !np) return;
  pilha_t *candidatos=NULL;
  aluno_t a;
  FILE *pf;
  for (int i=0; i<np; i++){
    le_aluno_txt(particoes[i],&a);
    candidatos = push_ordenado(candidatos,copia_aluno(&a),particoes[i]);
  }
  while(candidatos){
    pf = candidatos->f;
    salva_aluno_txt(f,candidatos->a);
    candidatos = pop(candidatos);
    le_aluno_txt(pf,&a);
    if (!feof(pf)){
      candidatos = push_ordenado(candidatos,copia_aluno(&a),pf);
    }
  }
}

////////////////////////////////////////////////////////////
typedef struct _no_abv{
  aluno_t *vencedor;
  struct _no_abv *p_vencedor;
  FILE *f;
  struct _no_abv *esq, *dir, *pai;
} abv_t;

typedef struct _fila_abv{
  abv_t *a;
  struct _fila_abv *prox;
} fila_abv_t;

fila_abv_t *insere_no_fim_fila(fila_abv_t *fila, abv_t *a){
  if (!a) return fila;
  fila_abv_t *novo = (fila_abv_t *)malloc(sizeof(fila_abv_t));
  if (fila){
    fila->prox=novo;
    fila=fila->prox;
  } else fila=novo;
  fila->a=a;
  fila->prox=NULL;
  return fila;
}

fila_abv_t *remove_do_inicio_fila(fila_abv_t *fila){
  if (!fila) return NULL;
  fila_abv_t *aux = fila;
  fila=fila->prox;
  free(aux);
  return fila;
}

abv_t *novo_no_arvbin_vencedores(abv_t *esq, abv_t *dir){
  if (!esq || !dir) return NULL;
  abv_t *novo = (abv_t *)malloc(sizeof(abv_t));
  if (esq->vencedor->nota < 11 && dir->vencedor->nota < 11){ // high value
    if (esq->vencedor->nota > dir->vencedor->nota){
      novo->vencedor = esq->vencedor;
      novo->p_vencedor = esq->p_vencedor;
      novo->f = esq->f;
    } else {
      novo->vencedor = dir->vencedor;
      novo->p_vencedor = dir->p_vencedor;
      novo->f = dir->f;
    }
  } else if (dir->vencedor->nota >= 11){
    novo->vencedor = esq->vencedor;
    novo->p_vencedor = esq->p_vencedor;
    novo->f = esq->f;
  } else {
    novo->vencedor = dir->vencedor;
    novo->p_vencedor = dir->p_vencedor;
    novo->f = dir->f;
  }
  novo->esq=esq;
  novo->dir=dir;
  novo->pai=NULL;
  esq->pai=novo;
  dir->pai=novo;
  return novo;
}

abv_t *cria_arvbin_vencedores(FILE **particoes, size_t np){
  if (!particoes || !np) return NULL;
  fila_abv_t *inicio=NULL, *fim=NULL;
  abv_t *folha;
  for (int i=0;i<np;i++){
    folha = (abv_t *)malloc(sizeof(abv_t));
    folha->vencedor = (aluno_t *)malloc(sizeof(aluno_t));
    le_aluno_txt(particoes[i],folha->vencedor);
    if (feof(particoes[i])) folha->vencedor->nota=11; // high value
    folha->p_vencedor = folha;
    folha->f = particoes[i];
    folha->pai=NULL;
    folha->esq=NULL;
    folha->dir=NULL;
    if (!inicio) {
      inicio = insere_no_fim_fila(NULL,folha);
      fim = inicio;
    } else fim = insere_no_fim_fila(fim,folha);
  }
  while (fim != inicio){
    fim = insere_no_fim_fila(fim,novo_no_arvbin_vencedores(inicio->a,inicio->prox->a));
    inicio=remove_do_inicio_fila(inicio);
    inicio=remove_do_inicio_fila(inicio);
  }
  abv_t *raiz = inicio->a;
  while(inicio) inicio=remove_do_inicio_fila(inicio);
  return raiz;
}

void atualiza_arvbin_vencedores(abv_t *a){
  if (!a) return;
  abv_t *no = a->p_vencedor;
  le_aluno_txt(no->f,no->vencedor);
  if (feof(no->f)) no->vencedor->nota=11; // high value
  while(no->pai){
    no=no->pai;
    if (no->esq->vencedor->nota < 11 && no->dir->vencedor->nota < 11){
      if (no->esq->vencedor->nota > no->dir->vencedor->nota){
        no->vencedor = no->esq->vencedor;
        no->p_vencedor = no->esq->p_vencedor;
        no->f = no->esq->f;
      } else {
        no->vencedor = no->dir->vencedor;
        no->p_vencedor = no->dir->p_vencedor;
        no->f = no->dir->f;
      }
    } else if(no->dir->vencedor->nota >= 11){
      no->vencedor = no->esq->vencedor;
      no->p_vencedor = no->esq->p_vencedor;
      no->f = no->esq->f;
    } else {
      no->vencedor = no->dir->vencedor;
      no->p_vencedor = no->dir->p_vencedor;
      no->f = no->dir->f;
    }
  }
}

void libera_arvbin_vencedores(abv_t *a){
  if (!a) return;
  if (!(a->esq) && !(a->dir)) free(a->vencedor);
  else{
    libera_arvbin_vencedores(a->esq);
    libera_arvbin_vencedores(a->dir);
  }
  free(a);
}

void intercala_particoes_arvbin_vencedores(FILE *f, FILE **particoes, size_t np){
  if (!f || !particoes || !np) return;
  abv_t *raiz = cria_arvbin_vencedores(particoes,np);
  while(raiz->vencedor->nota < 11){ // 11 sendo usado como "high value"
    salva_aluno_txt(f,raiz->vencedor);
    atualiza_arvbin_vencedores(raiz);
  }
  libera_arvbin_vencedores(raiz);
}
////////////////////////////////////////////////////////////
