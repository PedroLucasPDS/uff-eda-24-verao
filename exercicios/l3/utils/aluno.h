#ifndef MY_ALUNO_INCLUDED
#define MY_ALUNO_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _aluno{
  int mat;
  char nome[128];
  unsigned int nota;
} aluno_t;

void le_aluno_bin(FILE *f, aluno_t *a);
void salva_aluno_bin(FILE *f, aluno_t *a);

void le_aluno_txt(FILE *f, aluno_t *a);
void salva_aluno_txt(FILE *f, aluno_t *a);

void imprime_aluno(aluno_t *a);

size_t tamanho_arquivo_bin(FILE *f);
size_t linhas_arquivo_txt(FILE *f);

void imprime_conteudo_arquivo_bin(FILE *f);
void imprime_conteudo_arquivo_txt(FILE *f);

// OBS: Todas as funcoes abaixo visam ordenacao por nota, em ordem decrescente

void ordena_em_memoria_insertion_sort(FILE *f);
void ordena_em_memoria_selection_sort(FILE *f);

void ordena_em_disco_insertion_sort(FILE *f);
void ordena_em_disco_selection_sort(FILE *f);

size_t gera_particoes_ordenacao_interna(FILE *f, size_t n, size_t m);    // retornam numero de particoes geradas
size_t gera_particoes_selecao_substituicao(FILE *f, size_t n, size_t m);
size_t gera_particoes_selecao_natural(FILE *f, size_t n, size_t m);

void intercala_particoes_pilha(FILE *f, FILE **particoes, size_t np);
void intercala_particoes_arvbin_vencedores(FILE *f, FILE **particoes, size_t np);

#endif
