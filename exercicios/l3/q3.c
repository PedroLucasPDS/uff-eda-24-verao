#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/arvB.h"

int *arvb_impares_entre_xy(arvb_t *a, int x, int y, int *n_saida){
  return NULL; // placeholder. remover quando fizer o exercicio
  /*
  EXERCICIO: Completar
  */
}

int main(void){

  const int ordem = 3;

  arvb_t *raiz = arvb_inicia();
  
  raiz = arvb_insere(raiz,8,ordem);
  raiz = arvb_insere(raiz,1,ordem);
  raiz = arvb_insere(raiz,6,ordem);
  raiz = arvb_insere(raiz,3,ordem);
  raiz = arvb_insere(raiz,14,ordem);
  raiz = arvb_insere(raiz,36,ordem);
  raiz = arvb_insere(raiz,32,ordem);
  raiz = arvb_insere(raiz,43,ordem);
  raiz = arvb_insere(raiz,39,ordem);
  raiz = arvb_insere(raiz,41,ordem);
  raiz = arvb_insere(raiz,38,ordem);
  raiz = arvb_insere(raiz,4,ordem);
  raiz = arvb_insere(raiz,5,ordem);
  raiz = arvb_insere(raiz,42,ordem);
  raiz = arvb_insere(raiz,2,ordem);
  raiz = arvb_insere(raiz,7,ordem);
  
  arvb_imprime(raiz,0);
  
  // CHAMADA DA FUNCAO
  int n_saida=0;
  int x=4, y=41;
  int *v = arvb_impares_entre_xy(raiz,x,y,&n_saida);
  if (v){
    printf("\nimpares entre x e y:");
    for (int i=0; i<n_saida; i++) printf(" %d",v[i]);
    printf("\n");
    free(v);
  }
  
  arvb_libera(raiz);

  return 0;
}
