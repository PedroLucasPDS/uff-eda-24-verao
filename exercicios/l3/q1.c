#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/aluno.h"

typedef struct _aux_fila{ // struct para fila de particoes
  unsigned int n; // numero de registros na particao
  FILE *f;        // ponteiro para o arquivo
  struct _aux_fila *prox;
} fila_part_t;

fila_part_t *push_fim(fila_part_t *fila, unsigned int n, FILE *f){
  fila_part_t *novo = (fila_part_t *)malloc(sizeof(fila_part_t));
  novo->n=n;
  novo->f=f;
  novo->prox=NULL;
  if (fila) fila->prox=novo;
  return novo;
}

fila_part_t *pop_inicio(fila_part_t *fila){
  if (!fila) return NULL;
  fila_part_t *p = fila->prox;
  free(fila);
  return p;
}

size_t estima_custo_io_substituicao(size_t n, unsigned int m, unsigned int f){
  unsigned int part = (2*m);
  size_t np = 1 + (n-1) / part;
  size_t custo_io=n;
  if (np < f) return custo_io+n;
  fila_part_t *inicio=NULL, *fim=NULL;
  inicio=push_fim(NULL,part,NULL); fim=inicio;
  for (size_t i=1; i<np; i++) fim=push_fim(fim,part,NULL);
  for (long i=np; i>1; i-=(f-2)){
    fim=push_fim(fim,inicio->n,NULL);
    inicio=pop_inicio(inicio);
    for (unsigned int j=1; j<(f-1) && inicio != fim; j++){
      fim->n += inicio->n;
      inicio=pop_inicio(inicio);
    }
    custo_io += fim->n;
  } while(inicio) inicio=pop_inicio(inicio);
  return custo_io;
}

size_t estima_custo_io_natural(size_t n, unsigned int m, unsigned int f){
  return INT_MAX; // temporario. remover quando fizer o exercicio
   /*
  EXERCICIO: Completar
  */

}

void ordena_arq_txt(FILE *arq, unsigned int m, unsigned int f){
   /*
  EXERCICIO: Completar
  */

}

int main(void){

  char filename[] = "turma.txt";
  
  const unsigned int m = 4; // max de 4 registros em memoria simultaneamente
  const unsigned int f = 3; // max de 3 arquivos abertos simultaneamente
  
  FILE *arq = fopen(filename,"r+");
  if (!arq) {printf("Nao consegui abrir \"%s\"\n",filename); return 1;}
  ordena_arq_txt(arq,m,f);
  fclose(arq);

  return 0;
}
