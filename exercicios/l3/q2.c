#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/aluno.h"

void ordena_arq_bin_bubblesort(FILE *arq){
  /*
  EXERCICIO: Completar
  */
}

int main(void){

  char filename[] = "turma.bin";
  
  FILE *f = fopen(filename,"rb+");
  if (!f) {printf("Nao consegui abrir \"%s\"\n",filename); return 1;}
  ordena_arq_bin_bubblesort(f);
  fclose(f);

  return 0;
}
